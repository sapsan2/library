import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        Book book1 = new Book(1L, "Гарри Поттер", "Джоан Роулинг");
        Book book2 = new Book(2L, "унесенные призраками", "Хаяо Миядзаки");
        Book book3 = new Book(3L, "Гретель и Гензель", "Братья Гримм");

        ArrayList<Book> arrayList = new ArrayList<>();

        arrayList.add(book1);
        arrayList.add(book2);
        arrayList.add(book3);

        Library library = new Library("Библиотека", arrayList);

        Reader reader1 = new Reader("John", "Snow", "9111");

        while (true) {

            System.out.println("Если вы хотите забрать книгу, то введите число 1. \n" +
                    "Если вы хотите вернуть книгу, то введите число 2.");

            int operation = scanner.nextInt();

            if (operation == 1) {
                library.printBook();

                System.out.println("Введите номер книги, которую хотите забрать!");

                Long serialNumber = scanner.nextLong();

                library.takeBook(reader1, serialNumber);

                System.out.println();

                reader1.printTakenBook();
            } else if (operation == 2) {
                reader1.printTakenBook();

                System.out.println("Введите номер книги, которую хотите вернуть!");

                Long serialNumber = scanner.nextLong();

                library.returnBook(reader1, serialNumber);

                library.printBook();
            } else {
                System.out.println("Вы ввели неправильное число!");
            }

        }

    }

}