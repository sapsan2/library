import java.util.ArrayList;

public class Library {

    private String title;

    private ArrayList<Book> books;

    public Library(String title, ArrayList<Book> books) {
        this.title = title;
        this.books = books;
    }

    public void printBook() {
        System.out.println("Список книг: ");

        for (int i = 0; i < books.size(); i++) {
            System.out.println(books.get(i).getSerialNumber() + ": " + books.get(i).getTitle());
        }
    }

    public void takeBook(Reader reader, Long serialNumber) {
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getSerialNumber().equals(serialNumber)) {
                reader.getTakenBookList().add(books.get(i));

                System.out.println(reader.getName() + " взял книгу: " + books.get(i).getTitle());

                books.remove(books.get(i));
            }
        }
    }

    public void returnBook(Reader reader, Long serialNumber) {

        for (int i = 0; i < reader.getTakenBookList().size(); i++) {
            if (reader.getTakenBookList().get(i).getSerialNumber().equals(serialNumber)) {
                Book takenBook = reader.getTakenBookList().get(i);

                books.add(takenBook);

                System.out.println(reader.getName() + " вернул книгу: " + takenBook.getTitle());

                reader.getTakenBookList().remove(takenBook);
            }
        }


    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Book> getBooks() {
        return books;
    }

    public void setBooks(ArrayList<Book> books) {
        this.books = books;
    }
}
