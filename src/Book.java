public class Book {
    private Long serialNumber;

    private String title;

    private String author;

    public Book(Long serialNumber, String title, String author) {
        this.serialNumber = serialNumber;
        this.title = title;
        this.author = author;
    }

    public Long getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(Long serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
