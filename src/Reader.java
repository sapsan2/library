import java.util.ArrayList;

public class Reader {

    private String name;

    private String surname;

    private String readerNumber;

    private ArrayList<Book> takenBookList = new ArrayList<>();

    public Reader(String name, String surname, String readerNumber) {
        this.name = name;
        this.surname = surname;
        this.readerNumber = readerNumber;
    }


    public void printTakenBook() {
        System.out.println("Ваш список книг: ");
        for (int i = 0; i < takenBookList.size(); i++) {
            System.out.println(takenBookList.get(i).getSerialNumber() + ": " + takenBookList.get(i).getTitle());
        }
    }

    public ArrayList<Book> getTakenBookList() {
        return takenBookList;
    }

    public void setTakenBookList(ArrayList<Book> takenBookList) {
        this.takenBookList = takenBookList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getReaderNumber() {
        return readerNumber;
    }

    public void setReaderNumber(String readerNumber) {
        this.readerNumber = readerNumber;
    }
}

